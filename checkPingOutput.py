#!/bin/python
import sys
infilepath = sys.argv[1]
infile = open(infilepath, "r")
calib = 0
ctrl = 0
tbb = 0
for line in infile:

    if line.startswith("PINGING "):
        HFEC = line.split("PINGING ")[1].strip()
    if line.startswith("Slave 40"):
        calib = 1
        ctrl = 0
        tbb = 0
    elif (line.startswith("Slave 7e") or line.startswith("Slave 7f")):
        ctrl = 1
        calib = 0
        tbb = 0
    elif line.startswith("Slave 50"):
        tbb = 1
        calib = 0
        ctrl = 0

    if "TTCrx ID:" in line:
        hfstr=HFEC.ljust(15," ")
        if calib == 1:
            print(hfstr+" CALIB "+line.strip())
            calib = 0
        elif ctrl == 1:
            print(hfstr+" CTRL  "+line.strip())
            ctrl = 0
        elif tbb == 1:
            print(hfstr+" TBB   "+line.strip())
            tbb = 0

