import xmlrpc.client
dqmpassfile = "/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt"
with open(dqmpassfile, "r") as f:       
    cred = f.readline().strip()
dqmapi = xmlrpc.client.ServerProxy('https://'+cred+'@atlasdqm.cern.ch')
