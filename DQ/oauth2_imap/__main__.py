"""imap-oauth2 Entrypoint."""
from oauth2_imap.config import load_config
from oauth2_imap.imap_client import ImapClient


if __name__ == "__main__":
    conf = load_config()
    z = ImapClient(conf)
    z.process_mailbox()
