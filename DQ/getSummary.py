#!/bin/python3 
from argparse import ArgumentParser
from readMail import getMessages
import xmlrpc.client
import os, errno, sys, re
from datetime import datetime as dt
from email.mime.text import MIMEText
import smtplib
import string

from DQUtils import fetch_iovs
sys.path.append("/afs/cern.ch/user/l/larmon/public/prod/Misc")
from LArMonCoolLib import GetLBTimeStamps, GetOnlineLumiFromCOOL, GetReadyFlag
import DQDefects

Global_intol = ["DATACORRUPT", "RECOCORRUPT", "SPECIALSTUDIES", "BADTIMING", "LOWMUCONFIG_IN_HIGHMU_EmergencyMeasures"]
Part_intol = ["HVTRIP", "SEVNOISEBURST", "SEVCOVERAGE", "HVNONNOMINAL", "SEVNOISYCHANNEL", "SEVMISCALIB", "SEVUNKNOWN"]
Part_tol = ["COVERAGE", "HVNONNOM_CORRECTED"]

def sendMail(subject, contents):
    #print(sText)
    # send the email
    sFrom = os.environ["USER"]+" <"+os.environ["USER"]+"@cern.ch>"
    sTo = "atlas-lar-swdpdq-jira@cern.ch"
    print("*"*20)
    print("Proposed mail from", sFrom, "to", sTo, "subject is:", subject)
    print("Content:",contents)
    mail = MIMEText(contents)
    mail['From'] = sFrom
    mail['To'] = sTo # MUST HAVE THIS OR IT WON'T MAKE THE JIRA
    mail['Subject'] = subject
    smtp = smtplib.SMTP()
    send = input("Do you want to send? y or n \n")
    if send.lower() == "n":
        print("Will not send")
        return 
    elif send.lower() == "y":
        smtp.connect()
        smtp.sendmail(sFrom, sTo, mail.as_string())
        ##smtp.sendmail(sFrom, sTo, mail.as_string())
        smtp.close()
    print("*"*20)

def checkComment(msg, comments):
    def isSubStr(s1, s2):
        if isinstance(s1, dict):
            print("oioioio", s1)
        s1 = re.sub(r"[\n\t\s]*", "", s1)
        s1 = ''.join(e for e in s1 if e.isalnum())
        s2 = re.sub(r"[\n\t\s]*", "", s2)
        s2 = ''.join(e for e in s2 if e.isalnum())
        return s1 in s2
    for comm in comments:
        if isSubStr(msg, comm['body']):
            return True
    return False


def lbLengths(runNb, lumitag="OflLumiAcct-Run3-002"):
    v_lbTimeSt = GetLBTimeStamps(runNb)
    nLB = len(v_lbTimeSt)

    globalFilterDefects = ["GLOBAL_LHC_50NS","GLOBAL_LHC_NONSTANDARD_BC","GLOBAL_LHC_LOW_N_BUNCHES","GLOBAL_LHC_NOBUNCHTRAIN","GLOBAL_LHC_COMMISSIONING","GLOBAL_LHC_HIGHBETA","GLOBAL_LOWMUCONFIG_IN_HIGHMU","LUMI_VDM","GLOBAL_NOTCONSIDERED"]


    defectDatabase = DQDefects.DefectsDB(tag="HEAD")
    # Treatement of global filter
    globalFilterLB = []
    retrievedDefects = defectDatabase.retrieve((runNb, 1), (runNb, nLB), globalFilterDefects)
    for iRetrievedDefects in retrievedDefects:
        if (iRetrievedDefects.until.lumi > 4000000000):
            # The GLOBAL_NOTCONSIDERED defect is apparently set by default with a huge end of IOV.
            # BT October 2022 : this protection should be obsolete with the change of run 2 lines above. To be confirmed
            if (time.time()-v_lbTimeSt[len(v_lbTimeSt)][1]) > 48*3600: # During 48 hours after the end of run, the global filter is deactived to display all recent runs
                for lb in range(iRetrievedDefects.since.lumi,nLB+1):
                    globalFilterLB.append(lb)
        else:
            for lb in range(iRetrievedDefects.since.lumi,iRetrievedDefects.until.lumi):
                globalFilterLB.append(lb)

    # Atlas Ready
    atlasready=GetReadyFlag(runNb)
    readyLB=[]
    readyLB_globalFilter=[]
    for lb in list(atlasready.keys()):
        if (atlasready[lb]>0): readyLB+=[lb] 
        if (atlasready[lb]>0) and (lb not in globalFilterLB): readyLB_globalFilter+=[lb] 
    nLBready = float(len(readyLB_globalFilter))    

    thisRunPerLB = dict() # Contains various per LB run characteristics retrieved from COOL
    
    # COOL delivered luminosity
    
    thisRunPerLB["deliveredLumi"] = GetOnlineLumiFromCOOL(runNb,0)
    lumiacct = fetch_iovs('COOLOFL_TRIGGER::/TRIGGER/OFLLUMI/LumiAccounting', 
                          tag=lumitag,
                          since=v_lbTimeSt[1][0]*1000000000, 
                          until=v_lbTimeSt[len(v_lbTimeSt)][1]*1000000000) 
    thisRunPerLB['duration'] = dict()
    for iLumiAcct in range(len(lumiacct)):
        #thisRunPerLB['duration'][lumiacct[iLumiAcct].LumiBlock] = lumiacct[iLumiAcct].LBTime*lumiacct[iLumiAcct].LiveFraction
        thisRunPerLB['duration'][lumiacct[iLumiAcct].LumiBlock] = lumiacct[iLumiAcct].LBTime
    
    # Store the luminosity used for the efficiency normalisations
    for lb in range(1,nLB+2): # Loop on all LB - Protection to set a zero luminosity if not available
        if lb in readyLB_globalFilter:
            if lb not in list(thisRunPerLB["deliveredLumi"].keys()):
                thisRunPerLB["deliveredLumi"][lb] = 0.
                errorMsg = "Missing lumi for Run %d - LB %d\n"%(runNb,lb)
                print(errorMsg)
                errorLogFile.write(errorMsg)
            if lb not in list(thisRunPerLB["duration"].keys()):
                thisRunPerLB["duration"][lb] = 0.
                errorMsg = "Missing duration/LiveFraction for Run %d - LB %d\n"%(runNb,lb)
                print(errorMsg)
                errorLogFile.write(errorMsg)
        else:
            if lb not in list(thisRunPerLB["deliveredLumi"].keys()):
                thisRunPerLB["deliveredLumi"][lb] = 0.
            if lb not in list(thisRunPerLB["deliveredLumi"].keys()):
                thisRunPerLB["duration"][lb] = 0.
    return thisRunPerLB
            


def defectsSummary(dqmpassfile, runNumberList, skipUnchecked=False):
    with open(dqmpassfile, "r") as f:       
        cred = f.readline().strip()
    if cred is None:
        print("Problem reading credentials from",dqmpassfile)
    dqmapi = xmlrpc.client.ServerProxy('https://'+cred+'@atlasdqm.cern.ch')
    run_spec = {'stream': 'physics_CosmicCalo', 'source': 'tier0', 'run_list': runNumberList }
    run_info = dqmapi.get_run_information(run_spec)    
    # Number of LBs  print(run_info[str(runNumber)][6])
    defstrs = {}
    defdict = {}
    nLBs = {}
    for runNumber in runNumberList:
        #run_tag = run_info[str(runNumber)][1]
        #LBlens = lbLengths(runNumber)
        #sys.exit()
        if str(runNumber) in run_info.keys():
            nLBs[runNumber] = run_info[str(runNumber)][6]
        defects = dqmapi.get_defects_lb(run_spec,"", "HEAD", False, False, "Production", True)[str(runNumber)]
        lardef = [ k for k in defects.keys() if "lar" in k.lower() ]

        defstr = ""
        defdict[runNumber]={}
        for ld in lardef:        
            defdict[runNumber][ld] = []
            #if skipUnchecked and "UNCHECKED" in ld: continue        
            intol = False
            if any("_"+l in ld for l in Global_intol) or any ("_"+l in ld for l in Part_intol):
                intol = True
            thisdefstr = ""
            thisdefstr += "** Defect "+ld
            if intol is True:
                thisdefstr += "(INTOLERABLE)"
            thisdefstr += " **\n"
            defLB = []
            lrecov = 0
            lunrecov = 0
            for defect in defects[ld]:
                defLB.extend( list(range(defect[0], defect[1]+1)) )
                defdict[runNumber][ld].append(defect)
                #defstr += "In LB "+str(defect[0])+" - "+str(defect[1])
                #defstr += " "
                #defstr += "with comment: "+str(defect[3])+". "
                if defect[4] is True:
                    lrecov += (defect[1] - defect[0])+1
                else:
                    lunrecov += (defect[1] - defect[0])+1
            defLB = sorted(list(set(defLB)))
            thisdefstr += "Affecting "+str(len(defLB))+" LBs: "+", ".join([str(s) for s in defLB])+" ("+str(lrecov)+" recoverable) \n"
            if "UNCHECKED" not in ld: 
                defstr += thisdefstr
        defstrs[runNumber] = defstr
    return defstrs, defdict, nLBs


def writeToFile(msg, thefile, filename, thisjira=None):
    if thefile is None:
        print("Create file",filename)
        thefile = open(filename, "w")

    if thisjira is not None:
        thefile.write("Discussed in https://its.cern.ch/jira/browse/"+thisjira+"\n\n")

    thefile.write("== "+msg["type"]+" ("+msg["date"]+") ==\n")
    if isinstance(msg['content'], dict):
        for k in msg['content'].keys():
            thefile.write(k+":"+str(msg['content'][k])+"\n")
    else:
        thefile.write(msg['content']+"\n")
    if msg['cells'] is not None:
        thefile.write("\n*****************\n")
        thefile.write("The treated cells were:\n")
        thefile.write(("\n").join(msg['cells']))
    thefile.write("\n\n")
    return thefile


if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument('-r', '--run', dest='runNumber', help='Run number which you would like to get information for', type=int, nargs='+', required=True)
    parser.add_argument('-f', '--folder', dest='folder', default='inbox', help='Folder in your emails where the db expert update requests are found.')
    parser.add_argument('-p', '--passfile', dest='dqmpassfile', default="/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt", help='File containing atlasdqm credentials')
    parser.add_argument('-o', '--outdir', dest='outdir', default='signOffs', help='Output folder for .txt files containing summaries of sign-off activity')
    args = parser.parse_args()

    findJira = True
    jiras = None 

    try:
        os.makedirs(args.outdir)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

    messages = getMessages(runNumberList=args.runNumber, folder=args.folder)

    outfile_ES1 = {rn: None for rn in args.runNumber}
    outfile_BLK = {rn: None for rn in args.runNumber}
    outfile_Summary = {rn: None for rn in args.runNumber}
    details = {}
    for run in args.runNumber:
        details[run] = {"ES1":{}, "BLK":{}, "Comments":{}}

    contents = {}
    contents["Run"] = [str(run) for run in args.runNumber]
    if findJira:
        from findDoc import findJira, getComments
        # jira = findJira(run, cred[0], cred[1])
        jiras = [ findJira(run) for run in args.runNumber ]
        comments = [ getComments(j) if j is not None else [] for j in jiras ]
        contents["Jira"] = [ j.key if j is not None else "?" for j in jiras ]
        
        
    UPD3={}
    UPD4={}
    for msg in messages:
        thisrun = msg["runNumber"]
        if findJira:
            thisjira = jiras[args.runNumber.index(thisrun)]
            thesecomments = getComments(jiras[args.runNumber.index(thisrun)])
            if msg['fullcontent'] is not None:
                commented = checkComment(msg['fullcontent'], thesecomments)    
                tosend = msg['type']+" "+msg["source"]+"\n\n"+msg['fullcontent']
            else:
                commented = checkComment(msg['content'], thesecomments)
                tosend = msg['type']+" "+msg["source"]+"\n\n"+msg['content']
            #if not commented:
            #    sendMail(thisjira, tosend)
        
        date = dt.strptime(msg["date"].split(" +")[0],"%a, %d %b %Y %H:%M:%S")
        if "Noisy cells update" in msg["type"]:
            if msg["source"]=="BLK":
                if msg["runNumber"] not in UPD3.keys():
                    UPD3[msg["runNumber"]] = date
                elif date > UPD3[msg["runNumber"]]:
                    #print("YES UPD3",msg["runNumber"], date, UPD3[msg["runNumber"]])
                    UPD3[msg["runNumber"]] = date
                #else:
                    #print("NO UPD3",msg["runNumber"], date, UPD3[msg["runNumber"]])
            if msg["source"]=="ES1":
                if msg["runNumber"] not in UPD4.keys():
                    UPD4[msg["runNumber"]] = date
                elif date > UPD4[msg["runNumber"]]:
                    #print("YES UPD4",msg["runNumber"], date, UPD4[msg["runNumber"]])
                    UPD4[msg["runNumber"]] = date
                #else:
                    #print("NO UPD4",msg["runNumber"], date, UPD4[msg["runNumber"]])
    #print(UPD3)
    #print(UPD4)
    for msg in messages:
        if jiras is not None:
            thisjira = jiras[args.runNumber.index(msg["runNumber"])].key
        else:
            thisjira = None
        
        if "Noisy cells update" in msg["type"] and (msg["runNumber"] in UPD3.keys() or msg["runNumber"] in UPD4.keys() ):
            date = dt.strptime(msg["date"].split(" +")[0],"%a, %d %b %Y %H:%M:%S")
            if msg["runNumber"] in UPD3.keys() and msg["source"] == "BLK":
                if date != UPD3[msg["runNumber"]]:
                    print("This is an old",msg["runNumber"],"UPD3 message", date, UPD3[msg["runNumber"]])
                    continue
            if msg["runNumber"] in UPD4.keys() and msg["source"]=="ES1":
                if date != UPD4[msg["runNumber"]]:
                    print("This is an old",msg["runNumber"],"UPD4 message", date, UPD4[msg["runNumber"]])
                    continue
                else: 
                    if msg["runNumber"] == 428071:
                        print("YES", date, UPD4[msg["runNumber"]], msg["date"])
        if msg['source'] == "BLK":
            if isinstance(msg['content'], dict):
                details[msg['runNumber']]["BLK"].update(msg["content"])
            else: details[msg['runNumber']]["BLK"][msg['type']] = msg['content']
            print("Writing a BLK message:", msg['type'])
            outfile_BLK[msg['runNumber']] = writeToFile(msg, outfile_BLK[msg['runNumber']], args.outdir+"/BLK_"+str(msg['runNumber'])+".txt", thisjira)
        elif msg['source'] == "ES1":
            if isinstance(msg['content'], dict):
                details[msg['runNumber']]["ES1"].update(msg["content"])
            else: details[msg['runNumber']]["ES1"][msg['type']] = msg['content']
            print("Writing an ES1 message:", msg['type'])
            outfile_ES1[msg['runNumber']] = writeToFile(msg, outfile_ES1[msg['runNumber']], args.outdir+"/ES1_"+str(msg['runNumber'])+".txt", thisjira)
        else:
            print("WARNING Not sure where to put message:")
            print(msg)
    for rn in args.runNumber:
        if outfile_ES1[rn] is not None:
            outfile_ES1[rn].close()
        if outfile_BLK[rn] is not None:
            outfile_BLK[rn].close()
        
    print("Collecting defect info")
    defects, defdict, nLBs = defectsSummary(args.dqmpassfile, args.runNumber)

    for rn in args.runNumber:
        print("Create file",args.outdir+"/Summary_"+str(rn)+".txt")
        outfile_Summary[rn] = open(args.outdir+"/Summary_"+str(rn)+".txt", "w")
        if jiras is not None:
            thisjira = jiras[args.runNumber.index(rn)].key
            outfile_Summary[rn].write("Discussed in https://its.cern.ch/jira/browse/"+thisjira+"\n\n")
        outfile_Summary[rn].write(defects[rn])
        outfile_Summary[rn].close()

        

    tabfilename = args.outdir+"/table_"+("_").join([str(run) for run in args.runNumber])+".tex"
    csvfilename = args.outdir+"/table_"+("_").join([str(run) for run in args.runNumber])+".csv"
    print("Writing table to "+tabfilename)
    outfile_table = open(tabfilename, "w")
    outfile_csv = open(csvfilename, "w")

    

    contents["Express"] = [ "{\\textcolor{red}\\xmark}" if "LAR_FIRSTDEFECTS_UNCHECKED" in defdict[run] else "{\\textcolor{orange}\\cmark}" if "LAR_UNCHECKED" in defdict[run] else "{\\textcolor{green}\\cmark}" for run in args.runNumber ]
    contents["Express Noisy Cells"] = [ str(details[run]["ES1"]["SBN"])+" SBN, "+str(details[run]["ES1"]["HNHG"])+" HNHG" if "SBN" in details[run]["ES1"].keys() else "-" for run in args.runNumber ]
    contents["Bulk"] = [ "{\\textcolor{red}\\xmark}" if "LAR_BULK_UNCHECKED" in defdict[run].keys() else "{\\textcolor{green}\\cmark}" for run in args.runNumber ]
    contents["Bulk Noisy Cells"] = [ str(details[run]["BLK"]["SBN"])+" SBN, "+str(details[run]["BLK"]["HNHG"])+" HNHG" if "SBN" in details[run]["BLK"].keys() else "-" for run in args.runNumber ]


    for run in args.runNumber:
        for defect in list(defdict[run].keys()):
            if "UNCHECKED" in defect: continue
            nlb = sum([ (entry[1] - entry[0])+1 for entry in  defdict[run][defect] ])
            addstr = " ("+str(nlb)+" LBs)"
            tol = True
            if any("_"+l in defect for l in Global_intol) or any ("_"+l in defect for l in Part_intol):
                tol = False
            newdefname = defect
            if addstr != "":
                newdefname += addstr
            if tol is False:
                newdefname = "{\\textcolor{red}{"+newdefname+"}}"
            defdict[run][newdefname] = defdict[run][defect]
            #defdict[run][defect+addstr] = defdict[run][defect]
            del defdict[run][defect]
        if run in nLBs.keys():
            defdict[run]["(Run consists of "+str(nLBs[run])+" LBs)"] = ""
    
    defcolname = "Defects {\\textcolor{red}{(intol.)}}"
    contents[defcolname] = [ (", ").join([k.replace("_","\\_") for k in defdict[run].keys() if "UNCHECKED" not in k]) for run in args.runNumber]
   

    contents["Notes"] = []
    for run in args.runNumber:
        #if "Event Veto for run "+str(run) in details[run]["ES1"]:
        comment = ""
        if "Event Veto for run "+str(run) in details[run]["BLK"]:
            deets = details[run]["BLK"]["Event Veto for run "+str(run)]
            comment += "Noise/data corruption found offline: "
            for line in deets.split("\n"):
                if line.startswith("Found") and "project tag" not in line and "Noise or " not in line:
                    comment += line.split("Found ")[1].split(", covering")[0]+". "
                if "Lumi loss" in line and "Overall" not in line:
                    comment += line.replace("out of","/").replace("mini-noise-bursts","MNB").split("(")[0] + ". "
                    
        if "Missing EVs for run "+str(run) in details[run]["BLK"]:
            deets = details[run]["BLK"]["Missing EVs for run "+str(run)]
            comment += "Some EVs identified offline: "
            for line in deets.split("\n"):
                if line.startswith("Found a total of"):
                    comment += line.split("Found a total of ")[1].split(", covering")[0]+". "
                if "Lumi loss" in line and "Overall" not in line:
                    comment += line.replace("out of","/").replace("mini-noise-bursts","MNB").replace("noise-bursts","NB") + ". "
        if comment == "": comment = "-"
        contents["Notes"].append(comment)

    lines = ""
    table = '\\documentclass{article} \n'
    table += '\\usepackage{geometry} \n'
    table += '\\usepackage{pdflscape} \n'
    table += '\\usepackage{pifont} \n'
    table += '\\newcommand{\\cmark}{\\ding{51}}% \n'
    table += '\\newcommand{\\xmark}{\\ding{55}}% \n'
    table += '\\usepackage{tabularx} \n'
    table += '\\newcolumntype{L}{>{\\centering\\arraybackslash}X} \n'
    table += '\\usepackage{xcolor} \n'
    table += '\\usepackage{fontspec} \n'
    table += '\\setmainfont{Source Serif Pro}% Semibold} \n'
    table += '\\setsansfont{Source Sans Pro}% SemiBold} \n'
    table += '\\setmonofont{Source Code Pro}% Semibold} \n'
    table += '\\geometry{a4paper,left=10mm,right=10mm,top=10mm,bottom=20mm} \n'
    table += '\\begin{document} \n'
    table += '\\begin{landscape} \n'
    table += '\\centering \n'
    table += '\\begin{table} \n'
    table += '\\sffamily \n'
    table += '\\begin{tabularx}{\linewidth}{|'+('|').join(['c' if "Defects" not in k and "Notes" not in k else 'L' for k in contents.keys()]) + '|} \n'
    table += '\\hline \n'
    table += (" & ").join([ "\\bfseries{"+k+"}" for k in contents.keys() ]) +" \\\\ \n"
    lines += ",".join([k for k in contents.keys()])+"\n"
    table += '\\hline \n'
    for run in args.runNumber:
        r = args.runNumber.index(run)
        table += (" & ").join([contents[k][r] for k in contents.keys()]) + " \\\\ \n"
        lines += ",".join([contents[k][r].replace(","," -").replace("\\","").replace("{textcolor{green}cmark}","YES").replace("{textcolor{red}xmark}","NO") for k in contents.keys()])+"\n"
        csvJiraLink = '=HYPERLINK("https://its.cern.ch/jira/projects/ATLLARSWDPQ/issues/'+contents["Jira"][r]+'"),"'+contents["Jira"][r]+'")'
        lines = lines.replace(contents["Jira"][r], csvJiraLink)
        table += '\\hline \n'
        
    table += '\\end{tabularx} \n'
    table += '\\end{table} \n'
    table += '\\end{landscape} \n'
    table += '\\end{document} \n'

    outfile_table.write(table)
    outfile_table.close()

    outfile_csv.write(lines)
    outfile_csv.close()

