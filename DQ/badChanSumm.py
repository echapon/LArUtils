#!/bin/python3
# input the bad channel summary which comes from the weekly/monthly bad channel db email
import os, sys

if len(sys.argv) > 1:
    badChanFile = sys.argv[1]
    if not os.path.isfile(badChanFile):
        print("Requested file",badChanFile,"does not exist... please check")
        sys.exit()
else:
    print("Please provide a path to a bad channels file as a command line argument. If it is a file of SuperCells, also add a second argument of '1'")
    sys.exit()
isSC = False
if len(sys.argv) > 2:
    isSC = bool(sys.argv[2])


if isSC:
    onid = "SC_ONL_ID"
    print("Checking bad Super Cells list")
else:
    onid = "ONL_ID"
    print("Checking bad standard cells list")

print("Reading info from text file")
f = open(badChanFile, "r")
f = f.read()
channel=None
channels = {}
for line in f.split("\n"):
    line = line.strip()
    if line == "": continue
    if line.startswith("channel"):
        channel = line.split("channel=")[1].split(" ")[0]
        channels[channel] = {}
    else:
        if channel is not None:
            channels[channel][line.split("=")[0].strip()] = {}
            try:
                channels[channel][line.split("=")[0].strip()]["count"] = int(line.split("=")[1].split(" [")[0])
                channels[channel][line.split("=")[0].strip()]["runs"] = line.split("[")[1].split("]")[0].replace("'","").strip(" ").split(",")
            except Exception as e:
                print(e)
                print(line)
                sys.exit()
                
        

PrintMappingPath = "../printMapping.py"

want = [ onid, "DET", "AC", "DETNAME", "FT", "SL", "CH", "SAM", "FEB", "HVCORR" ]

print("Getting info from LArIdTranslator")
cmd = "python3 "+PrintMappingPath+" -w "+(" ").join(want)
out = os.popen(cmd).read()
pmout = {}
for line in out.split("\n"):
    line = line.strip().split(" ")
    if len(line) == 1: continue
    onind = want.index(onid)
    pmout[int(line[onind],base=16)] = dict(zip([x for i,x in enumerate(want) if i!=onind],[x for i,x in enumerate(line) if i!=onind]))
print("Also getting HV line info")
hvcmd = "python3 "+PrintMappingPath+" -w "+onid+" HVLINES"
hvout = os.popen(hvcmd).read()
for line in hvout.split("\n"):
    line = line.strip().split(" ")
    if len(line) == 1: continue
    onid = int(line[0], base=16)
    pmout[onid]["HVLINES"] = line[1:]

hvcounts = {}
FEBcounts = {}
spBNcounts = {}
hnHGcounts = {}
for chan in channels.keys():
    intchan = int(chan,base=16)
    print("*"*10)
    print(chan, intchan)
    for k in channels[chan].keys(): print(k, ":", channels[chan][k])
    for k in pmout[intchan].keys(): print(k, ":", pmout[intchan][k])

    try:
        hnHGcounts[intchan] = channels[chan]["hnHG"]["count"]
        spBNcounts[intchan] = channels[chan]["spBN"]["count"]
    except Exception as e:
        print(e)
        print("****", channels[chan])


    FEB = pmout[intchan]["FEB"]
    if FEB not in FEBcounts.keys():
        FEBcounts[FEB] = 1
    else:
        FEBcounts[FEB] += 1
    
    hvlines = pmout[intchan]["HVLINES"]
    for hv in hvlines:
        if hv not in hvcounts.keys():
            hvcounts[hv] = 1
        else:
            hvcounts[hv] += 1

def getFirstN(mydict, n):
    return  {k: mydict[k] for k in list(mydict)[:n]}

hvcounts_sorted = {k: v for k, v in sorted(hvcounts.items(), key=lambda item: item[1], reverse=True)}
FEBcounts_sorted = {k: v for k, v in sorted(FEBcounts.items(), key=lambda item: item[1], reverse=True)}
hnHGcounts_sorted = {k: v for k, v in sorted(hnHGcounts.items(), key=lambda item: item[1], reverse=True)}
spBNcounts_sorted = {k: v for k, v in sorted(spBNcounts.items(), key=lambda item: item[1], reverse=True)}
topN = 5
topHV = getFirstN(hvcounts_sorted, topN)
topFEB = getFirstN(FEBcounts_sorted, topN)
print(topN,"HV lines with most frequently noisy cells:")
print("\n".join([str(k)+" : "+str(v) for k,v in topHV.items()]))
print(topN,"FEBs with most frequently noisy cells:")
print("\n".join([str(k)+" : "+str(v) for k,v in topFEB.items()]))
tophnHG = getFirstN(hnHGcounts_sorted, topN)
topspBN = getFirstN(spBNcounts_sorted, topN)
print(topN,"channels with most hnHG flags:")
print("\n".join([str(k)+" : "+str(v) for k,v in tophnHG.items()]))
print(topN,"channels with most spBN flags:")
print("\n".join([str(k)+" : "+str(v) for k,v in topspBN.items()]))
