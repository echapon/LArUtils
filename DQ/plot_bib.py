import xmlrpc.client
import argparse
import datetime

###################################################
###          F  U  N  C  T  I  O  N  S          ###
###################################################

def parse_html(file, s):
    '''
    File parser

    Arguments:
    - file (string): path to HTML file with WDE output for noisy cells for express for a given run
      It HAS to be named as <runnb>.html
    - s (xmlrpc.client.ServerProxy): for getting run information
    '''
    lines = []
    with open(file, errors='replace') as f:
        lines = f.readlines()

    lines_since_last_cluster=-1
    bibclusters=0

    runnb = file.split('/')[-1]
    runnb=runnb.split('.')[0]
    print(runnb)
    run_spec = {'stream': 'physics_CosmicCalo', 'source': 'tier0', 'run_list': [runnb]}
    infokeys = ["Run type", "Project tag", "Partition name", "Number of events passing Event Filter", "Run start", "Run end", "Number of luminosity blocks", "Data source", "Detector mask", "Recording enabled", "Number of events in physics streams" ]
    beamkeys = ["Max beam energy during run", "Stable beam flag enabled during run", "ATLAS ready flag enabled during run", "Total integrated luminosity", "ATLAS ready luminosity (/nb)"]
    run_info = s.get_run_information(run_spec)
    for ri in run_info.keys():
        run_info[ri] = { ik:li for ik,li in zip(infokeys,run_info[ri]) }
    beam_info = s.get_run_beamluminfo(run_spec)
    for bi in beam_info.keys():
        beam_info[bi] = { ik:li for ik,li in zip(beamkeys,beam_info[bi]) }

    lumi=beam_info[runnb]["ATLAS ready luminosity (/nb)"]
    date_start=datetime.datetime.fromtimestamp(run_info[runnb]["Run start"])
    fill=0 #FIXME

    for l in lines:
        if not "at position" in l:
            if lines_since_last_cluster>=0 and "0x" in l and not "HEC" in l and not "FCAL" in l:
                lines_since_last_cluster = lines_since_last_cluster+1
            continue
        if "to cure" in l:
            continue
        l = l.split('at position')[1]
        l = l.split('(')[1].split(')')[0]
        eta = float(l.split(',')[0].strip())
        phi = float(l.split(',')[1].strip())
        if lines_since_last_cluster>=0:
            #print(eta, phi, lines_since_last_cluster)
            if abs(eta)<3 and abs(phi)<0.2 and lines_since_last_cluster>5:
                bibclusters = bibclusters+1
        lines_since_last_cluster=0

    #print(eta, phi, lines_since_last_cluster)
    #print(sys.argv[1], date, time, lumi, bibclusters, file=sys.stderr)
    ans = {"runnb": runnb, "date_start": date_start, "lumi": lumi, "bibclusters": bibclusters, "fill": fill}
    print(ans)
    return ans

###################################################
###                   M  A  I  N                ###
###################################################

parser = argparse.ArgumentParser(description='Beam-induced background.')
parser.add_argument('inputs', type=str, nargs='+', help='1 HTML page / run (saved from WDE)')
parser.add_argument('--runnb', action='store_true', help='Use run nb. instead of date')
parser.add_argument('--fills', action='store_true', help='Use fill nb. instead of date')
parser.add_argument('--bylumi', action='store_true', help='Divide number of clusters by lumi')
parser.add_argument('--dqmpassfile', type=str, help='DQM pass file', default='dqmpass.txt')

args = parser.parse_args()

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime
import numpy as np

dqmpassfile = args.dqmpassfile
cred = None
with open(dqmpassfile, "r") as f:
    cred = f.readline().strip()
if cred is None:
    print("Problem reading credentials from",dqmpassfile)
s = xmlrpc.client.ServerProxy('https://'+cred+'@atlasdqm.cern.ch')


runs = []
dates = []
lumis = []
nclus = []
lines = []
fills = []

use_runnb = args.runnb
use_fills = args.fills

for file in args.inputs:
    d = parse_html(file, s)
    dates.append(d["date_start"])
    lumis.append(d["lumi"]/1000.) # nb-1 to pb-1
    n = d["bibclusters"]
    if args.bylumi:
        n = n/lumis[-1]
    nclus.append(n)
    runs.append(d["runnb"])
    fills.append(d["fill"])

fig, ax1 = plt.subplots()

if not use_fills and not use_runnb:
   locator = mdates.AutoDateLocator(minticks=3, maxticks=7)
   formatter = mdates.ConciseDateFormatter(locator)
   ax1.xaxis.set_major_locator(locator)
   ax1.xaxis.set_major_formatter(formatter)

color = 'tab:red'
if use_runnb:
    if not use_fills:
        ax1.set_xlabel('Run nb')
    else:
        ax1.set_xlabel('LHCfill')
else:
    ax1.set_xlabel('Date')
ax1.set_ylabel('Lumi [pb-1]', color=color)
ax1.plot(dates, lumis, color=color,marker='+')
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
ax2.set_ylabel('# (noise clusters, |phi|<0.2) / L [pb]', color=color)  # we already handled the x-label with ax1
ax2.plot(dates, nclus, color=color,marker='+')
ax2.tick_params(axis='y', labelcolor=color)
ax2.set_ylim(0,0.4)

# annotate
offset = 72
bbox = dict(boxstyle="round", fc="0.8")
arrowprops = dict(
    arrowstyle="->",
    connectionstyle="angle,angleA=0,angleB=90,rad=10")
#  ax1.annotate(
#          'last run before LHC issue',
#          lastbefore,
#          xytext=(-offset, offset), textcoords='offset points',
#          bbox=bbox, arrowprops=arrowprops)
#  firstafter = (firstafter[0], 100)
#  ax1.annotate(
#          'first run after LHC issue',
#          firstafter,
#          xytext=(offset, -offset), textcoords='offset points',
#          bbox=bbox, arrowprops=arrowprops)
# mp = (firstafter[0]+lastbefore[0])/2
# ax2.plot([mp,mp],[0,0.4],'-', color='black')
# ax2.annotate(
#         'LHC vacuum issue',
#         (mp,0.39),
#         xytext=(20, 20), textcoords='offset points',
#         bbox=bbox, arrowprops=arrowprops)

if not use_runnb:
    # run numbers
    for i in range(len(runs)):
        ax2.text(dates[i], nclus[i]+0.01, str(runs[i]))
else:
    if not use_fills:
        ax1.set_xticks(np.arange(len(runs)), labels=runs)
        ax2.set_xticks(np.arange(len(runs)), labels=runs)
    else:
        ax1.set_xticks(np.arange(len(runs)), labels=fills)
        ax2.set_xticks(np.arange(len(runs)), labels=fills)
    labels = ax1.get_xticklabels()
    plt.setp(labels, rotation=45, horizontalalignment='right')
    labels = ax2.get_xticklabels()
    plt.setp(labels, rotation=45, horizontalalignment='right')


fig.tight_layout()  # otherwise the right y-label is slightly clipped
# plt.show()
plt.savefig('bib.pdf')
