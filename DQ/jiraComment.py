#!/bin/python3 
from jira import JIRA

def sendMail(subject, contents, verbose=False):
    sFrom = os.environ["USER"]+" <"+os.environ["USER"]+"@cern.ch>"
    sTo = "atlas-lar-swdpdq-jira@cern.ch"
    if verbose is True:
        print("*"*20)
        print("Proposed mail from", sFrom, "to", sTo, "subject is:", subject)
        print("Content:",contents)
    mail = MIMEText(contents)
    mail['From'] = sFrom
    mail['To'] = sTo # MUST HAVE THIS OR IT WON'T MAKE THE JIRA
    mail['Subject'] = subject
    smtp = smtplib.SMTP()
    smtp.connect()
    smtp.sendmail(sFrom, sTo, mail.as_string())
    smtp.close()


def findRunJira(runNb, verbose=False):
    tokenfile = "jiraToken"
    with open(tokenfile, "r") as f:       
        token = f.readline().strip()
    j=JIRA("https://its.cern.ch/jira", token_auth=(token))
    project = 'ATLLARSWDPQ'
    issues = j.search_issues('project=' + project + ' AND summary~"DQ Assessment for run '+str(runNb)+'"', maxResults=False, fields = 'comment')
    if verbose: print(issues)    
    if len(issues) == 1:
        return issues[0]
    elif len(issues) > 1:
        if verbose: print("more than one issue found")
        return None
    else:
        if verbose: print("No issue found")
        return None
        

def getComments(issue):
    if issue is None: return None
    comments = issue.raw['fields']['comment']['comments']
    return comments

def checkComment(msg, comments):
    def isSubStr(s1, s2):
        if isinstance(s1, dict):
            print("oioioio", s1)
        s1 = re.sub(r"[\n\t\s]*", "", s1)
        s1 = ''.join(e for e in s1 if e.isalnum())
        s2 = re.sub(r"[\n\t\s]*", "", s2)
        s2 = ''.join(e for e in s2 if e.isalnum())
        return s1 in s2
    for comm in comments:
        if isSubStr(msg, comm['body']):
            return True
    return False

def checkRunComments(run, contents):
    issue = findRunJira(run)
    if issue is not None:
        comments = getComments(issue.key)
        commented = checkComment(contents, comments)
        if not commented:
            print("Will send mail")
    else:
        print("Could not find JIRA for", run)
