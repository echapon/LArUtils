# checkSignOff.py
Prints the signoff status of a defined set of runs. For help, run:

python3 checkSignoff.py -h   

Example: 
python3 checkSignoff.py -n 900 --onlyLAr --onlySB

To see the latest-900 runs where LAr was enabled, stable beam only. 


# getSummary.py
Produces .txt files containing information which was sent to the db experts by LADIeS and offline event veto scripts. For help, run:

python3 getSummary.py -h

Example:
python3 getSummary.py -r 429027 427884 -f "UPD Proposal"

To produce summaries for the indicated list of runs. The -f argument reads from a specific mail folder. Default would be inbox. 

