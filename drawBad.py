import sys,os
import printMapping
from argparse import ArgumentParser
import pandas as pd
from PulsePattern import makePlot




if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-i', '--inputFile', dest='infile', help='Input bad SC file', type=str, required=True)
    parser.add_argument('-isSC', dest='isSC', action='store_true', default=False, help='SuperCells?')

    parser.add_argument('-v', '--verbose', default=False, action='store_true')
    args = parser.parse_args()

    if args.isSC:
        onlid = "SC_ONL_ID"
        eta = "SCETA"
        phi = "SCPHI"                
    else:
        onlid = "ONL_ID"
        eta = "ETA"
        phi = "PHI"

    query = [ onlid, eta, phi, "SAM", "DETNAME", "QUADRANT", "FEB", "CALIB", "FTNAME" ]
    if args.isSC: query.extend(["LATOME_NAME", "LATOME_FIBRE", "LTDB"])
    criteria = {"want":query}
    pmdf = pd.DataFrame(printMapping.query(**criteria), columns=query)
    pmdf = pmdf.set_index(onlid)
    

    if not os.path.isfile(args.infile):
        print("Requested file",args.infile,"does not exist")
        sys.exit()
    
    onlIDs = []
    coords = []
    with open(args.infile, "r") as inf:
        
        for line in inf.readlines():
            ID = line.split("# ")[1].split(" ->")[0]
            reasons = line.split("  # ")[0].split(" 0 ")[-1]
            row = pmdf.loc[ID]
            thiseta = row[eta]
            thisphi = row[phi]
            thissam = row["SAM"]
            if not isinstance(thiseta, str):
                thiseta = thiseta.iloc[0]
                thisphi = thisphi.iloc[0]
                thissam = thissam.iloc[0]
                print(ID, ":", reasons, "("+", ".join([str(c)+":"+str(row.iloc[0][c])  for c in row.keys()])+")")
            else:
                print(ID, ":", reasons, "("+", ".join([str(c)+":"+str(row[c]) for c in row.keys()])+")")
            coords.append((float(thiseta), float(thisphi), int(thissam)))
            
    makePlot(coords, "all", args.infile.split(".")[0], True)

    
