import sys
import printMapping as pm
import numpy as np


def minNSF(inputlist):
    ''' Round the lists to the minimum number of significant figures that still keeps the items distinct '''
    nsf = 2
    for n in range(nsf, 12):
        newlist = [ float(np.format_float_positional(l, precision=int(n), unique=False, fractional=False, trim='k')) for l in inputlist]
        if len(inputlist) != len(list(set(newlist))):
            #print(n, len(inputlist), len(list(set(newlist))))
            continue
        else:
            # print("NSF",n)
            return newlist

    return newlist


def binsFromList(vals):
    vals = minNSF(vals)

    def absdiff( a, b ):
        mx = max(abs(a), abs(b))
        mn = min(abs(a), abs(b))
        return (mx-mn)

    bins = []
    for phi in vals:
        p = vals.index(phi)
        if p == 0:
            bins.append(0)
        elif p < len(vals)-1:
            #val1 = phi - (absdiff(vals[p+1], phi)/2)
            val = phi - (absdiff(vals[p-1], phi)/2)
            if val < bins[p-1] and bins[p-1] != 0:
                print("NONO", val, bins[p-1])
                print(vals[p-1], phi, vals[p+1])
                sys.exit()
            bins.append( val )


    bins[0] = vals[0] - (absdiff(bins[1],vals[0])/2)
    bins = minNSF(bins)
    return(bins)


#DET = "FCALA"
DET = "EMBA"
SAM = 1

want = ['DETNAME', 'SCETA', 'SCPHI']
have = ['DET', 'SAM']
have_val = [DET, SAM]

query = pm.query(**{'want':want, 'have':have, 'have_val':have_val})


phis = sorted(list(set([float(q[want.index('SCPHI')]) for q in query])))
etas = sorted(list(set([float(q[want.index('SCETA')]) for q in query])))

phibins = binsFromList(phis)
etabins = binsFromList(etas)

def printGaps(bins):
    gaps = []
    for bi in bins:
        b = bins.index(bi)
        try:
            gaps.append(bins[b+1] - bi)
        except:
            continue
    print(minNSF(gaps))
    
print(len(phibins),"PHI BINS:", phibins)
#printGaps(phibins)
print(len(etabins), "ETA BINS:", etabins)
#printGaps(etabins)
