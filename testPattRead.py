#bool LArCalibParams::CalibBoard::isPulsed(const unsigned event,  const unsigned short lineNumber) const
#{ //The pattern is changed after nTrigger events.
#const unsigned PatternStepSize=m_nTrigger;
#unsigned iPattern=(event/PatternStepSize)%(m_Pattern.size()/4);
#iPattern*=4; //A pattern consists of 4 words (4*32bit= 128 bit)
#const unsigned nWord=lineNumber>>5;  //lineNumber/32;
#const unsigned Pattern=m_Pattern[iPattern+nWord];
#const unsigned nBit=lineNumber&0x1f; //lineNumber%32;
#return Pattern>>nBit & 0x1;
#}

def getPulsed(line):
    v = []
    for i in range(0,32):
        print(line)
        if len(line)==0: continue
        #if (int(line[3].split("0x")[1],32) & (1 << i)): 
        if (int(line[3],16) & 1 << i): 
            print("****",line[3],"3", i, int(line[3],16), int(line[3].split("0x")[1],32))
            v.append(i)
        #if (int(line[2].split("0x")[1],32) & (1 << i)): 
        if (int(line[2],16) & 1 << i): 
            print("****",line[2], "2",i+32)
            v.append(i + 32)
        #if (int(line[1].split("0x")[1],32) & (1 << i)): 
        if (int(line[1],16) & 1 << i): 
            print("****",line[1], "1", i + 64)
            v.append(i + 64)
        #if (int(line[0].split("0x")[1],32) & (1 << i)): 
        if (int(line[0],16) & 1 << i): 
            print("****",line[0], "0", i +96)
            v.append(i + 96)
    return v

def getBinary(my_hexdata):
    scale = 16 ## equals to hexadecimal
    num_of_bits = 32
    return bin(int(my_hexdata, scale))[2:].zfill(num_of_bits)
    
test="0x0003000f"
print("OIOIOIO", getBinary(test))
# 110000000000001111
# 196623
# 0 1 2 3
testint = int(test.split("0x")[1],32)
testint2 = int(test,16)
print(testint)
print(testint2)

t = 1
def bitset(number, n):
    """Test whether ``number`` has the ``n``'th bit set"""
    return bool(number & 1 << n )
for i in range(0, 32):
    print(i, bitset(testint2, i), bitset(testint, i))


line = "0x03000300 0x0003000f 0x33000300 0x0003000f"
#getPulsed(line.split(" "))
line=line.split(" ")
for l in range(0, len(line)):
    word = line[l]
    print(word)
    thisint = int(word,16)
    thisdec = getBinary(word)
    n = 32*(3 - l)
    print(word, thisint, thisdec, "+"+str(n))
    revdec = list(reversed(thisdec))
    print("**", [ int(t)+n for t in range(0,32) if int(revdec[t])==1 ] )
