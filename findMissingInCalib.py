#!/bin/python3
import ROOT as R
import sys,os
import glob
import printMapping
import pandas as pd

def getFileStruct(infile):
    struct = {}
    treenames = [k.GetName() for k in infile.GetListOfKeys()]
    for tr in treenames:        
        tree = infile.Get(tr)
        struct[tr] = [k.GetName() for k in tree.GetListOfBranches()]
    return struct


def main(infile, isSC, pmdf, thisRunType=None):
    run = infile.split("/")[-1].split("_")[1]
    runtype = infile.split("/")[-1].split("_")[0]
    infilename = infile
    infile = R.TFile(f,"READ")            
    struct = getFileStruct(infile)
    
    allchan = []
    if thisRunType is not None:
        if runtype != thisRunType:
            return runtype, allchan
    
    if isSC:
        onlIdName = "SC_ONL_ID"
    else:
        onlIdName = "ONL_ID"
    pmdf = pmdf.astype({onlIdName:'int'})

    if not isSC:
        part = infile.split("/")[-1].split("_")[2].split("EB-")[1]
        part = part.split(".root")[0]
        if "LArPed" in runtype:
            pmdfcut = pmdf
        elif part.startswith("EMB"):
            pmdfcut = pmdf.loc[(pmdf['DETNAME'] == part) & (pmdf['SAM'] != '0')]
        elif part.startswith("PS"):
            parts = [ "EMBA", "EMBC"]
            pmdfcut = pmdf.loc[(pmdf['DETNAME'].isin(parts)) & (pmdf['SAM'] == '0')]
        elif "EMEC" not in part:
            parts = [ part+"A", part+"C" ]
            pmdfcut = pmdf.loc[(pmdf['DETNAME'].isin(parts)) & (pmdf['SAM'] == '0')]
        else:
            print("please define",part)
            pmdfcut = pmdf.loc[(pmdf['DETNAME'] == part)]
    else:
        pmdfcut = pmdf
        part = "-"
    pmchan = list(set(pmdfcut[onlIdName]))
    cols = list(pmdf.columns)


    for tree in struct.keys():
        if 'channelId' not in struct[tree]:
            print("Cannot find channelId branch in tree",tree)
            continue
        channelIds = list(set(R.RDataFrame(infile.Get(tree)).AsNumpy(['channelId'])['channelId']))
        allchan.extend(channelIds)
        #print(len(channelIds), "channels in",tree,"tree")
        #print(len(channelIds), len(pmdfcut))
        if len(channelIds) != len(pmdfcut):
            extra = list(set(channelIds) - set(pmchan))
            missing = list(set(pmchan) - set(channelIds))
            if len(missing) == 0 and len(extra) == 0: continue
            print('-'*40)
            print("Run",run, "part",part, "type", runtype, "file", infilename)
            print(len(missing), "missing", len(extra), "extra in",tree,"tree")

            if not isSC:
                if len(extra) > 0 and "LArPed" not in runtype:
                    #print(len(channelIds), len(pmchan))
                    if pd.isna(pmdf.loc[pmdf[onlIdName].isin(extra)]) is False:
                        print("**** THE EXTRA CELLS ****")
                        extras = pmdf.loc[pmdf[onlIdName].isin(extra)]
                        print(', '.join( [c+" "+str(extras[c].nunique()) for c in cols if c != onlIdName]) )
                        for c in cols:
                            nunique = extras[c].nunique()
                            if nunique < 20:
                                print(nunique, c+"s:",(", ").join(extras[c].unique()))
                if len(missing) > 0:
                    print("**** THE MISSING CELLS ****")
                    missings = pmdf.loc[pmdf[onlIdName].isin(missing)]
                    print(', '.join( [c+" "+str(missings[c].nunique()) for c in cols if c != onlIdName]) )
                    for c in cols:
                        nunique = missings[c].nunique()
                        if nunique < 20:
                            print(nunique, c+"s:",(", ").join([str(s) for s in missings[c].unique()]))
                        else:
                            print(nunique, c)

            else:
                print("**** THE PRESENT CELLS ****")
                present = pmdf.loc[pmdf[onlIdName].isin(channelIds)]
                for c in cols:                    
                    nunique = present[c].nunique()
                    if nunique < 20:
                        print(nunique, c+"s:",(", ").join([str(s) for s in present[c].unique()]))
                    else:
                        print(nunique, c)
                
    infile.Close()
    return runtype, list(set(allchan))

if __name__ == "__main__":
    if len(sys.argv) > 1:
        indir = sys.argv[1]
    else:
        indir = "/eos/project/a/atlas-larcalib/AP/00442943_00442948_00442949_EMB-EMEC_HIGH_1/root_files/"
    print("*"*10,"Processing",indir,"*"*10)
    if len(sys.argv) > 2:
        isSC = bool(sys.argv[2])
    else:
        isSC = False

    if not os.path.isdir(indir):
        print("The provided input directory does not exist")
        sys.exit()
    # info from laridtranslator
    if isSC is False:
        want = ["ONL_ID","DETNAME","SAM","FEB","QUADRANT", "CALIB"]
    else:
        want = ["SC_ONL_ID","DETNAME","SAM","FEB","QUADRANT", "CALIB", "LATOME_NAME", "LATOME_FIBRE", "LTDB"]
    inputs={"want":want,"showdecimal":True}
    print("querying printMapping for",want)
    pmout = printMapping.query(**inputs)
    pmdf = pd.DataFrame(pmout, columns = want)
    files = glob.glob(indir+"/*.root")


    allchan = {}
    for f in files:
        if "/RootHistos" in f: continue
        if not any(st in f for st in ["LArRamp", "LArPed", "LArCali", "OFCCali"]): continue
        thistype, thesechan = main(f, isSC, pmdf)
        if thistype not in allchan.keys():
            allchan[thistype] = thesechan
        else:
            allchan[thistype].extend(thesechan)


    if isSC:
        onlIdName = "SC_ONL_ID"
    else:
        onlIdName = "ONL_ID"
    pmdf = pmdf.astype({onlIdName:'int'})
    pmchan = list(set(pmdf[onlIdName]))
    cols = list(pmdf.columns)

    for runtype in allchan.keys():
        thesechan = allchan[runtype]
        print("-*-"*20)
        print(runtype)
        print(len(thesechan))
        print(len(list(set(thesechan))))

        extra = [e for e in list(set(thesechan) - set(pmchan)) if int(e) != 0]
        missing = [m for m in list(set(pmchan) - set(thesechan)) if int(m) != 0]
        
        print(len(pmchan), len(extra), "extra",  len(missing), "missing")

        if len(extra) > 0:
            #print(len(channelIds), len(pmchan))
            if pd.isna(pmdf.loc[pmdf[onlIdName].isin(extra)]) is False:
                print("**** THE EXTRA CELLS ****")
                extras = pmdf.loc[pmdf[onlIdName].isin(extra)]
                print(', '.join( [c+" "+str(extras[c].nunique()) for c in cols if c != onlIdName]) )
                for c in cols:
                    nunique = extras[c].nunique()
                    if nunique < 20:
                        print(nunique, c+"s:",(", ").join(extras[c].unique()))
        if len(missing) > 0:
            print("**** THE MISSING CELLS ****")
            print(missing)
            missings = pmdf.loc[pmdf[onlIdName].isin(missing)]
            pd.set_option('display.max_rows', missings.shape[0]+1)
            print(missings)
            for c in cols:
                nunique = missings[c].nunique()
                if nunique < 20:
                    print(nunique, c+"s:",(", ").join([str(s) for s in missings[c].unique()]))
                else:
                    print(nunique, c)







