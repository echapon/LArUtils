#!/bin/python
# Translate pulse patters, with info about number of pulsed cells
# Author: Ellis Kay <ellis.kay@cern.ch>
# Date: 14/12/2020
################################################################################

# TO DO
# Add full pulse pattern list (i.e. order of pulsing)
# Add input arguments to query given cell/SC pulsed
# Pattern writer? Show neighbouring cells?


import sys,os
import printMapping
from argparse import ArgumentParser
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from matplotlib.ticker import MaxNLocator


def getlist( cmd ):
    # print( cmd )
    translator=os.popen(cmd).read()
    layout = []
    if "ERROR" in translator:
        print(translator)
        sys.exit()
    for feb in translator.strip().split('\n'):
        if feb.strip() != "":
            line = feb.strip().split(" ")
            layout.append(line)
    return layout

def getEtaPhiRange(partition,SC=False):
    if partition.lower() == "all":
        criteria = {}
    else:
        criteria = {"have":["DET"],"have_val":[partition]}
    
    if SC:
        criteria["want"] = ["SCETA","SCPHI"]
    else:
        criteria["want"] = ["ETA","PHI"]
    criteria["showdecimal"] = True
    result = printMapping.query(**criteria)
    etas = list(set([ float(r[0]) for r in result ]))
    phis = list(set([ float(r[1]) for r in result ]))
    etas = [ e for e in etas if int(e)!=-9999 ]
    phis = [ p for p in phis if int(p)!=-9999 ]
    return min(etas),max(etas),len(etas),min(phis),max(phis),len(phis)


class patternReader:
    #bool LArCalibParams::CalibBoard::isPulsed(const unsigned event,  const unsigned short lineNumber) const
    #{ //The pattern is changed after nTrigger events.
    #const unsigned PatternStepSize=m_nTrigger;
    #unsigned iPattern=(event/PatternStepSize)%(m_Pattern.size()/4);
    #iPattern*=4; //A pattern consists of 4 words (4*32bit= 128 bit)
    #const unsigned nWord=lineNumber>>5;  //lineNumber/32;
    #const unsigned Pattern=m_Pattern[iPattern+nWord];
    #const unsigned nBit=lineNumber&0x1f; //lineNumber%32;
    #return Pattern>>nBit & 0x1;
    #}
    def getPulsed(self, line):
        v = []
        for i in range(0,32):
            if len(line)==0: continue
            if (int(line[0],16) & (1 << i)): 
                #print("****",line[0],"3", i, int(line[0],16), int(line[0].split("0x")[1],32))
                v.append(i)
            if (int(line[1],16) & (1 << i)): 
                #print("****",line[1], "1",i+32)
                v.append(i + 32)
            if (int(line[2],16) & (1 << i)): 
                #print("****",line[2], "2", i + 64)
                v.append(i + 64)
            if (int(line[3],16) & (1 << i)): 
                #print("****",line[3], "3", i +96)
                v.append(i + 96)
        return v

    def __init__(self, infile):
        self.infile = infile
        self.data = {}
        self.defs = [ "nDAC","DAC","nDel","Del","nPattern" ]

        with open(self.infile, "r") as f:            
            for linenum,line in enumerate(f,0):
                if linenum < len(self.defs):
                    self.data[self.defs[linenum]] = [ int(s) for s in line.strip().split() ]
                else:
                    self.data["PatternLines "+str(linenum-len(self.defs)+1)] = line.strip().split()
                    self.data["Pattern "+str(linenum-len(self.defs)+1)] = self.getPulsed(line.strip().split())
                    

    def getData(self):
        return self.data

    def print_data(self):
        for k in self.data.keys():
            if "Pattern" in k:
                print( k.rjust(9), ":", self.data[k], "( len =",len(self.data[k]),")")
            else:
                print( k.rjust(9), ":", self.data[k])

    def print_var(self,query):
        patternlines = self.data
        results = []
        for k in patternlines.keys():
            if k.startswith("Pattern "):
                for CL in patternlines[k]:
                    for partition in ["EMBA","EMBC","EMECA","EMECC","HECA","HECC","FCALA","FCALC"]:
                        criteria = {"have":["DET","CL"], "have_val":[partition, str(CL)], "want":query}
                        criteria["showdecimal"] = True
                        result = printMapping.query(**criteria)
                        print(result)
                        sys.exit()


def makePlot(coords, partition, name, SC=False):
    # Plotting
    etas = [ coords[ind][0] for ind,x in enumerate(coords) ]
    phis = [ coords[ind][0] for ind,x in enumerate(coords) ]
    lays = [ coords[ind][2] for ind,x in enumerate(coords) ]
    fig = plt.figure(figsize=(10,10))
    ax = plt.axes(projection='3d')
    ax.set_xlabel('eta')
    ax.set_ylabel('phi')
    ax.set_zlabel('layer')
    etamin,etamax,neta,phimin,phimax,nphi = getEtaPhiRange(partition,SC)
    #print(etamin,etamax,neta,phimin,phimax,nphi)
    if not SC: neta/=9
    if not SC: nphi/=8
    etabins = np.arange(etamin,etamax,(etamax-etamin)/neta)
    phibins = np.arange(phimin,phimax,(phimax-phimin)/nphi)     
    ax.set_xlim(etamin,etamax)    
    ax.set_ylim(phimin,phimax)
    ax.set_zlim(0,3)
    ax.set_xticks(etabins)
    ax.set_yticks(phibins)
    ax.tick_params(axis='x', labelrotation=45, labelsize=9)
    ax.tick_params(axis='y', labelrotation=-45, labelsize=9)

    for label in ax.xaxis.get_ticklabels()[::2]:
        label.set_visible(False)
    for tick in ax.xaxis.get_major_ticks():
        tick.set_pad(0)
    for label in ax.yaxis.get_ticklabels()[::2]:
        label.set_visible(False)
    for tick in ax.yaxis.get_major_ticks():
        tick.set_pad(0)

    #colour1=(1/256, 2/256, 4/256, 5/256)
    #colour2=(1/256, 2/256, 4/256, 5/256)
    
    #colourmap = np.array([1/256, 2/256, 4/256, 5/256])
    layers = np.array(lays,dtype='i')

    ax.zaxis.set_major_locator(MaxNLocator(integer=True))
    ax.scatter3D(np.array(etas,dtype='d'),
                 np.array(phis,dtype='d'),
                 np.array(lays,dtype='d'),
                 # c=colourmap[layers],
                 c=layers,
                 cmap='hsv',
                 vmin=0, vmax=3,
                 s=.5 )

    # 'gray')
    if "," in partition: partition = partition.replace(",","-")
    print("Saving figure:",name+"_"+partition+".png")
    plt.savefig(name+"_"+partition+".png", bbox_inches='tight')





if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-i', '--inputFile', dest='patternfile', help='Input pattern file', type=str, required=True)
    parser.add_argument('-p', '--partition', dest='partition', nargs='+', help='The partition that the pattern applies to (check one partition at a time', type=str, default="EMBA")
    parser.add_argument('-doPlot', dest='doPlot', action='store_true', default=False, help='Add this argument to make a plot showing the eta phi map for the pattern')
    parser.add_argument('-v', '--verbose', default=False, action='store_true')
    args = parser.parse_args()

    if args.verbose:
        print("*"*30)
        print("Pattern file = "+args.patternfile)
        print("Partition(s)    = "+", ".join(args.partition))
        print("*"*30)

    #printList = "Supercells"
    printList = "LAr Cells"

    patterns = patternReader(args.patternfile)
    if args.verbose: patterns.print_data()
    patternlines = patterns.getData()
    if args.verbose: print("")

    allquery = ["SC_ONL_ID", "ONL_ID"]

    allpm = printMapping.query(**{'want':allquery, 'showdecimal':True})
    SCs = list(set([ap[0] for ap in allpm]))
    SCdict = { SC: [ ap[1] for ap in allpm if ap[0] == SC ] for SC in SCs }

    # Print some info from the LArID db
    #query = [ "FEB","LATOME_NAME","SAM","SC_ONL_ID","SCETA","SCPHI","ONL_ID","ETA","PHI" ] # "LTDB"
    query = [ "FEB","SAM","SC_ONL_ID","SCETA","SCPHI","ONL_ID","ETA","PHI" ] # "LTDB"


    allcoords = []
    allcoordsSC = []

    for partition in args.partition:
        coords = {}
        coordsSC = {}

        total = {}

        TotalFullyPulsedSC = 0
        allTargetSCs = []
        for k in patternlines.keys():
            if k.startswith("Pattern "):
                fullyPulsedSC = 0
                coords[k] = []
                coordsSC[k] = []

                targetSCs = []
                targetCells = []
                fullSC = []
                for CL in patternlines[k]:
                    criteria = {"have":["DET","CL"], "have_val":[partition, str(CL)], "want":query, "showdecimal":True}
                    result = printMapping.query(**criteria)
                    count={}
                    allcount={}
                    countstr=[]
                    coords[k].extend([(sublist[query.index("ETA")],
                                       sublist[query.index("PHI")],
                                       sublist[query.index("SAM")]) for sublist in result ])
                    coordsSC[k].extend([(sublist[query.index("SCETA")],
                                       sublist[query.index("SCPHI")],
                                       sublist[query.index("SAM")]) for sublist in result ])
                    for q in query:
                        if "ONL_ID" in q or "FEB" in q or "LTDB" in q or "LATOME" in q or "SAM" in q:
                            count[q] = list(set([ sublist[query.index(q)] for sublist in result ]))
                            allcount[q] = [ sublist[query.index(q)] for sublist in result ]
                            if (q=="FEB" or q=="LTDB"): name = q+"s"
                            elif q == "SAM": name = "Layers"              
                            elif "LATOME" in q: name = "LATOMEs"
                            elif "SC" in q: name = "Supercells"
                            elif q == "ONL_ID": name = "LAr Cells"
                            else: name = q

                            if q == "SAM":
                                countstr.append(str(len(count[q])).rjust(4)+" "+name+" ("+(", ").join(count[q])+")")
                            else:
                                countstr.append(str(len(count[q])).rjust(4)+" "+name+ "("+str(len(allcount[q]))+")")

                            if q not in total.keys(): total[q] = []    
                            total[q].extend(count[q])

                            
                            if args.verbose:
                                if printList and name == printList:
                                    print("List of "+printList+": "+(", ").join(count[q]))
                    if "SC_ONL_ID" in count.keys() and "ONL_ID" in count.keys():
                        targetSCs.extend(count["SC_ONL_ID"])
                        targetCells.extend(count["ONL_ID"])
                        for SC in count["SC_ONL_ID"]:
                            exp = SCdict[SC]
                            pulsed = [ c for c in exp if c in count["ONL_ID"] ]
                            #if len(exp) == len(pulsed):
                                #print("SC",SC,"was fully pulsed")
                            #    fullyPulsedSC+=1
                            #    TotalFullyPulsedSC+=1
                            #else:
                                #print("SC",SC,"NOT fully pulsed -",len(pulsed),"/",len(exp))

                    if args.verbose:
                        if len(countstr)!=0:
                            print("Calibration Line",CL,":",(", ").join(countstr))

                
                if args.doPlot:
                    makePlot(coords[k], partition, k.replace(" ","_"))
                    makePlot(coordsSC[k], partition, k.replace(" ","_")+"_SC",True)

                #fullyPulsedSC = 0
                targetSCs = list(set(targetSCs))
                allTargetSCs.extend(targetSCs)
                targetCells = list(set(targetCells))
                for SC in targetSCs:
                    exp = SCdict[SC]
                    pulsed = [ c for c in exp if c in targetCells ]
                    if len(exp) == len(pulsed):
                        #if args.verbose: "SC",SC,"was fully pulsed")
                        fullSC.append(SC)
                        fullyPulsedSC+=1
                        TotalFullyPulsedSC+=1
                    else:
                        if args.verbose: print("SC",SC,"NOT fully pulsed -",len(pulsed),"/",len(exp), "- pulsed:",pulsed, "not:",[e for e in exp if e not in pulsed], k)
                if len(targetSCs) > 0:
                    print("*"*20)
                    print(fullyPulsedSC, "SCs were fully pulsed in this line")
                    if args.verbose: print("Fully pulsed SCs are:",(", ").join(fullSC))
                    print("*"*20)
        for q in total.keys():
            total[q] = list(set(total[q]))
            print(len(total[q]), q)
        
        allcoords.extend([ l for sublist in list(coords.values()) for l in sublist])
        allcoordsSC.extend([ l for sublist in list(coordsSC.values()) for l in sublist])

        print("*"*20)
        print(TotalFullyPulsedSC, "SCs were fully pulsed in total (out of", len(SCdict.keys()),"possible SCs,",len(list(set(allTargetSCs))),"targeted SCs)")
        print("*"*20)
    if args.doPlot:
        makePlot(allcoords, ",".join(args.partition), "all")
        makePlot(allcoordsSC, ",".join(args.partition), "all_SC",True)
    


