#!/bin/python3
from datetime import datetime 

infile = open("dump.txt","r")
outfile = open("parsedDump.txt", "w")
outfile.write("Date, TT, Partition, Run, Beam Status \n")

TTs = []
parts = []
dates = [] 
stabledates = []
beams = []
for line in infile.readlines():
    date = line.split(",")[0].split(":",2)[-1]
    dates.append( datetime.strptime(date, '%Y-%b-%d %H:%M:%S') )
    line = line.split("FAILED TO FIND NOISY CELL in ")[1].split(". This application will no longer")[0]
    TT = line.split("tower ")[1].split(" located")[0]
    part = line.split("located in ")[1].split(".")[0]
    Run = line.split("Run number = ")[1].split(";")[0]
    Beam = line.split("Beam state = ")[1].split(".")[0]
    if "stable" not in Beam.lower(): continue
    stabledates.append( datetime.strptime(date, '%Y-%b-%d %H:%M:%S') )
    #print(date, TT, part, Run, Beam)
    newline = ", ".join([date, TT, part, Run, Beam])+"\n"
    beams.append(Beam)
    outfile.write(newline)
    TTs.append(TT)
    parts.append(part)
infile.close()
outfile.close()

setTTs = list(set(TTs))
setparts = list(set(parts))
print("Data taken from",min(dates),"to",max(dates))
print("Occurences between",min(stabledates),"and",max(stabledates))
print("In state(s):",", ".join(list(set(beams))))
print( len(setTTs), "TTs in", len(setparts), "partitions. Counts are:")
for TT in setTTs:
    print(TT,":",TTs.count(TT))
print("See parsedDump.txt for details")

