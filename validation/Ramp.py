import sys, os
import uproot4 as U
import datetime as dt
import plotter
import numpy as np


def process(infilename, run, SCinfo, outdir=".", thresholds=None):

    run = infilename.split("/")[-1].split("_")[1].split(".root")[0]
    infile = U.open(infilename)
    treename = "RAMPS"
    posstrees = [ t for t in infile.keys() if t == treename or t == treename+";1" ]
    if len(posstrees) == 0:
        print("ERROR: tree", treename, "is not in the file",infilename)
        sys.exit()
    elif len(posstrees) > 1:
        print("WARNING: tree", treename, "may have a duplicate:", posstrees)
    tree = infile[posstrees[0]] 
    tree = tree.arrays(library="np")

    print("Available branches:",tree.keys())
    
    stats = plotter.getStats(tree)
    print(stats.keys())

    if thresholds is None:
        thresholds = {}
    detHists={}
    outliers = {}
    maxval = {}
    minval = {}
    for det in stats["poss_detector"]:
        if det == 2: continue  # same plot for IW and OW EMEC
        for side in stats["poss_pos_neg"]:
            for lay in stats["poss_layer"]:
                #if plotter.detKey[det] == "HEC":
                #    hkey = str(det)+"_"+str(side)
                #else:
                hkey = str(det)+"_"+str(side)+"_"+str(lay)
                if hkey in detHists.keys(): continue
                detHists[hkey]={}
                '''
                if "ped" not in thresholds.keys():
                    thresholds["ped"] = [800, 1200]
                if "ped" not in outliers.keys():
                    outliers["ped"] = {}
                    maxval["ped"] = {}
                    minval["ped"] = {}
                outliers["ped"][hkey] = []
                minval["ped"][hkey] = [1000000000000,0]
                maxval["ped"][hkey] = [-1000000000000,0]
                
                detHists[hkey]["ped"] = plotter.InitEtaPhiHist(det, side, lay, "Pedestal", "Pedestal", thresholds["ped"][0], thresholds["ped"][1], SCinfo)
                if detHists[hkey]["ped"] is None:
                    del detHists[hkey]
                    continue
                if "rms" not in thresholds.keys():
                    thresholds["rms"] = 0.3, 1.2
                if "rms" not in outliers.keys():
                    outliers["rms"] = {}
                    maxval["rms"] = {}
                    minval["rms"] = {}
                outliers["rms"][hkey] = []
                minval["rms"][hkey] = [1000000000000,0]
                maxval["rms"][hkey] = [-1000000000000,0]

                detHists[hkey]["rms"] = plotter.InitEtaPhiHist(det, side, lay, "PedestalRMS", "Pedestal RMS", thresholds["rms"][0], thresholds["rms"][1], SCinfo)
                '''

    channels = list(dict.fromkeys(tree["channelId"]))
    print(len(channels), "channels")
    nwarn=0
    dets = []

    for chanID in channels:
        events = np.where( tree["channelId"] == chanID )[0]  # Need the 0 because it returns a tuple
        eta = str([ tree["eta"][ev] for ev in events ][0])
        phi = str([ tree["phi"][ev] for ev in events ][0])
        layer = str([ tree["layer"][ev] for ev in events ][0])
        detector = str([ tree["detector"][ev] for ev in events ][0])
        side = str([ tree["pos_neg"][ev] for ev in events ][0])
                
        plotter.checkSCinfo(layer, "SAM", SCinfo[chanID], nwarn<20)
        plotter.checkSCinfo(detector, "DET", SCinfo[chanID], nwarn<20)

        print("OIOIOI",len(tree["X"]),[ tree["X"][ev] for ev in events if len(tree["X"][ev])>0], [ tree["X"][ev] for ev in events ])
        sys.exit()

        #print(len(events))       
        thisdet = str(detector)
        if detector not in dets: dets.append(detector)
        if thisdet == "2": thisdet = "1"
        hkey = str(thisdet)+"_"+str(side)+"_"+str(layer)

        # Loop over the variables and fill the histograms
        for var in detHists[hkey].keys():
            val = sum( [tree[var][ev] for ev in events] ) / len(events)
            thisx = detHists[hkey][var].GetXaxis().FindBin(abs(float(eta)))
            thisy = detHists[hkey][var].GetYaxis().FindBin(abs(float(phi)))
            thisz = detHists[hkey][var].GetBinContent(thisx,thisy)
            # Collect information about outliers, max & min
            if val < thresholds[var][0] or val > thresholds[var][1]:
                outliers[var][hkey].append([val,chanID])

            if val < minval[var][hkey][0]:
                minval[var][hkey] = [val, chanID]
            if val > maxval[var][hkey][0]:
                maxval[var][hkey] = [val, chanID]

            if thisz != 0 and val != thisz:
                if nwarn < 30: 
                    print("WARNING, variable",var,"bin",thisx, thisy, "already filled with",thisz,"for detector", plotter.detKey[int(detector)]+plotter.sideKey[int(side)], "layer", str(layer), "eta",eta,"phi",phi,str(chanID), str(hex(chanID)), "(want to fill with", str(val)+")")
                elif nwarn == 30: print("Suppressing further warnings about filled bins")
                nwarn+=1
            detHists[hkey][var].Fill(abs(float(eta)), float(phi), val)


    plotter.plotDetHists(detHists, run, outdir, outliers, maxval, minval, SCinfo)
    #print("oi",dets)

if __name__ == "__main__":
    started = dt.datetime.now()
    print("Started Pedestal job at "+str(started) )
    
    
    if len(sys.argv) < 2:
        print("Pass the input file as an argument")
        sys.exit()
    infile = sys.argv[1]
    if len(sys.argv) > 2:
        outdir  = sys.argv[2]
    else:
        outdir = "plots_"+infile.split("/")[-1]
    plotter.chmkDir(outdir)

    from steer import getSCinfo

    process(infile, getSCinfo, outdir)
