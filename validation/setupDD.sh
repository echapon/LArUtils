#!/bin/sh

thisLCG=/cvmfs/sft.cern.ch/lcg/views/LCG_101_ATLAS_2/x86_64-centos7-gcc11-opt/setup.sh

echo source ${thisLCG}
source ${thisLCG}
pyver=$(python --version | cut -d' ' -f2 | cut -f1,2 -d'.')
echo "Python version is ${pyver}"
initial="$(echo $USER | head -c 1)"
myPy=/afs/cern.ch/user/${initial}/${USER}/.local/lib/python${pyver}/site-packages


libs=( uproot4 awkward1 )
for lib in ${libs[@]}; do
    test=$(pip3 list --disable-pip-version-check --format=columns | grep -F ${lib})
    if [ -z "${test}" ]; then
	echo "pip3 install ${lib} --user"
	pip3 install ${lib} --user > /dev/null 2>&1
    fi
done

