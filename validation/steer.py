import os, sys
import datetime as dt
import platform
import Pedestal, CaliWave, Ramp
import plotter


def defineThresholds():
    thresholds = {}
    thresholds["Pedestal"] = {}
    thresholds["Pedestal"]["ped"] = [750, 1100]
    thresholds["Pedestal"]["rms"] = [0.2, 1.0]
    thresholds["CaliWave"] = None
    thresholds["OFCCali"] = None
    thresholds["Ramp"] = None
    for runtype in thresholds.keys():
        if thresholds[runtype] is None: continue
        print("Thresholds for",runtype,"are:")
        for k in thresholds[runtype].keys():            
            print(k,":",thresholds[runtype][k])
        
    return thresholds

def getSCinfo():
    PrintMappingDir = "/afs/cern.ch/user/e/ekay/LAr/LArUtils/"
    variables = [ "SC_ONL_ID", "SCNAME", "FT", "SL", "SAM", "SCETA", "SCPHI", "FTNAME", "LTDB", "SCNAME", "QUADRANT", "LATOME_NAME", "DET", "AC" ]
    cmd="python3 "+PrintMappingDir+"printMapping.py -w "+" ".join(variables)+" -dec"
    out=os.popen(cmd).read()

    outdict = {}
    for line in out.split("\n"):
        if line == "": continue
        vals = line.strip().split(" ")
        if vals[variables.index("SCNAME")] == "NOT_CONNECTED": continue
        try:
            scid = int(vals[0])
        except Exception as e:
            print(e)
            print(vals)
            sys.exit()
        if scid not in outdict.keys():
            outdict[scid] = {k:v for k,v in zip(variables[1:],vals[1:])}            
            if "HEC" in vals[variables.index("LATOME_NAME")]:
                outdict[scid]["SAM"] = [outdict[scid]["SAM"]]

        else:
            if "HEC" in vals[variables.index("LATOME_NAME")]:
                outdict[scid]["SAM"].append(vals[variables.index("SAM")])
            else:
                print("NONONONO duplicate",vals)
                print(outdict[scid])
    return outdict

if __name__ == "__main__":
    started = dt.datetime.now()
    print("Started at "+str(started) )
    ver = platform.python_version()

    if len(sys.argv) < 2:
        print("Pass the input directory as an argument")
        sys.exit()
    indir = sys.argv[1]
    if len(sys.argv) > 2:
        outdir  = sys.argv[2]
    else:
        outdir = "plots"

    plotter.chmkDir(outdir)

    thresholds = defineThresholds()


    infiles = [ f for f in os.listdir(indir) if f.endswith(".root") ]

    peds = sorted(list([ f for f in infiles if "LArPedAutoCorr" in f and "RootHistos" not in f ]))

    caliwaves = sorted(list([ f for f in infiles if "LArCaliWave" in f ]))

    ofccalis = sorted(list([ f for f in infiles if "LArOFCCali" in f ]))

    ramps = sorted(list([ f for f in infiles if "LArRamp" in f ]))

    print("Getting SC info from LArId.db")
    SCinfo = getSCinfo()
    print( "Process took",str(dt.datetime.now()-started) )

    for pedestal in peds:
        run = pedestal.split("_")[-1].split(".root")[0]
        Pedestal.process(indir+"/"+pedestal, run, SCinfo, outdir, thresholds["Pedestal"])
    for caliwave in caliwaves:
        run = caliwave.split("_")[-1].split(".root")[0]
        #CaliWave.process(indir+"/"+caliwave, run, SCinfo, outdir, thresholds["CaliWave"])
    for ramp in ramps:
        run = ramp.split("_")[-1].split(".root")[0]
        #Ramp.process(indir+"/"+ramp, run, SCinfo, outdir, thresholds["Ramp"])
