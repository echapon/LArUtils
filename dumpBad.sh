#!/bin/sh

isSC=0
if [[ $# -lt 1 ]]; then
    echo "Please provide the run number as an argument"
fi

runNumber=$1

if [[ $# -gt 1 ]]; then
    isSC=$2
fi

echo "RUN $runNumber isSC? $isSC"

#AthenaCmd=Athena,master,latest
AthenaCmd=22.0.45,Athena

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
	
ascmd="source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh "
echo ${ascmd}
eval ${ascmd}

export AtlasSetup=/afs/cern.ch/atlas/software/dist/AtlasSetup/scripts
acmd="source ${AtlasSetup}/asetup.sh ${AthenaCmd}"
echo ${acmd}
eval ${acmd}



if [ $isSC -eq 1 ]; then
    outname="badSC_${runNumber}"
    fulltag=`getCurrentFolderTag.py "COOLOFL_LAR/CONDBR2" /LAR/BadChannelsOfl/BadChannelsSC` 
else
    outname="badChan_${runNumber}"
    fulltag=`getCurrentFolderTag.py "COOLOFL_LAR/CONDBR2" /LAR/BadChannelsOfl/BadChannels` 
fi
upd4TagName=`echo $fulltag | grep -o "UPD4-[0-9][0-9]"` 

for folder in ${upd4TagName} Bulk-00 UPD1-00 UPD4-00; do
    thisoutname=${outname}_${folder}.txt
    if [ $isSC -eq 1 ]; then
	athcmd="athena.py -c 'IOVEndRun=${runNumber};OutputFile=\"${thisoutname}\";tag=\"LARBadChannelsOflBadChannelsSC-RUN3-${folder}\"; folderStr=\"/LAR/BadChannelsOfl/BadChannelsSC\"' LArBadChannelTool/LArBadChannel2Ascii.py"
    else
	athcmd="athena.py -c 'IOVEndRun=${runNumber};OutputFile=\"${thisoutname}\";tag=\"LARBadChannelsOflBadChannels-RUN2-${folder}\"' LArBadChannelTool/LArBadChannel2Ascii.py"
    fi
    echo ${athcmd}
    eval ${athcmd}
    echo "Check output in file ${thisoutname}"
done

which athena.py
