#!/bin/python3 

import imaplib
import email
from email.header import decode_header
import webbrowser
import os, sys
import smtplib
from getpass import getpass, getuser
from argparse import ArgumentParser

def parseEVMail(contents):
    source = ""
    if "RUN2-Bulk" in contents:
        source = "BLK"
    elif "RUN2-UPD4" in contents:
        source = "ES1"
    contents = contents.replace("=\r\n","")
    #contents = contents.replace("=\n","")   
    contents = contents.replace("=3D","=")   
    contents = contents.replace("^M","")   
    if "To run the update:" in contents:
        return source, contents.split("To run the update:")[0]

    else: return source, contents

def parseMissingEVMail(contents, runNumber):
    source = "BLK" #### is it?!?!?
    contents = "EVs in the local sqlite files which were not in the uploaded db\n"+contents
    contents = contents.split("EventVeto"+str(runNumber)+"_Missing.db")[1]
    contents = contents.split("veto length")[0]+"veto length"
    contents = contents.replace("=\r\n","")
    #contents = contents.replace("=\n","")
    contents = contents.replace("=","=")   
    contents = contents.replace("^M","")   
    return source, contents

def parseUPDMail(contents):   
    if "no UPD3 update is needed for run" in contents or "no UPD4 update is needed for run" in contents:
        return "No cells were flagged as noisy by the LAr DQ shifters for this run", None, None
        
    delim = "=3D"

    toread = {}
    
    contents = contents.replace("=\r\n","")
    contents = contents.replace("=\n","")
    contents = contents.replace("by \nLADIeS","by LADIeS")
    contents = contents.replace("-> \n","-> ")
    contents = contents.replace("by \r\nLADIeS", "by LADIeS")
    WDEstring = "WDE:WebDisplayExtractor"
    if "WDE:dev version" in contents:
        WDEstring = "WDE:dev version"
    try:
        contents = WDEstring+contents.split(WDEstring)[1]
    except Exception as e:
        print(e)
        print(contents)
        sys.exit()

    for line in contents.split("\n"):
        if line.startswith("Total number of"):
            thisline = line.split("Total number of ")[1].strip()
            category = thisline.split(":")[0]
            try:
                number = thisline.split(":")[1]
            except Exception as e:
                print("uh oh",thisline)
                print()
                print(repr(contents))
                sys.exit()
            if "channels flagged" in category:
                toread["Flagged cells"] = int(number.split("(")[0])
                toread["Flagged in PS"] = int(number.split("(")[1].split("in")[0])
            elif "unflagged" in category:
                toread["Unflagged by DQ shifters"] = int(number)
            elif "modified" in category:
                if "-> sporadicBurstNoise" in category:
                    toread["Changed to SBN"] = int(number)
                elif "-> highNoise" in category:
                    toread["Changed to HNHG"] = int(number)
            elif "sporadicBurstNoise" in category:
                toread["SBN"] = int(number.split("(")[0])
                toread["SBN in PS"] = int(number.split("(")[1].split("in")[0])
            elif "highNoiseHG" in category:
                toread["HNHG"] = int(number.split("(")[0])
                toread["HNHG in PS"] = int(number.split("(")[1].split("in")[0])
        elif line.startswith("Cluster matching"):
            toread["Cluster matching"] = line.split("Cluster matching")[1]
        elif line.startswith("WDE:Web"):
            toread["Tool version"] = line.split("WDE:")[1]
        else: continue


    cellList = None
    if "Sincerely yours" in contents:
        contents = contents.split("Sincerely yours")[0]
    if "=3D=3D=3D=3D=3D=3D=3D=3DPense bete" in contents:
        contents = contents.split("=3D=3D=3D=3D=3D=3D=3D=3DPense bete")[0]
    if delim not in contents:
        delim = "="
    if delim in contents:
        cellList = contents.rsplit(delim,1)[1].strip().split("\n")
        if len(cellList) > 0:
            if 'flagged' in toread.keys():
                if len(cellList) != toread['flagged']:
                    print("WARNING: The reported number of flagged cells is not the same as the cell list size... please check")
    contents = contents.replace("=3D","=")   
    return toread, cellList, contents


def connectIMAP(server="imap.cern.ch", returncred=False):
    username = getuser()
    print("Please provide password for",username)
    password = getpass()
    # create an IMAP4 class with SSL 
    imap = imaplib.IMAP4_SSL(server)
    # authenticate
    try:
        imap.login(username, password)
    except Exception:
        password = getpass()
        imap.login(username, password)
    if returncred:
        return imap, [username,password]
    else:
        return imap

def getMessages(runNumberList=[999999], folder="inbox", local=False, mailmax=20000, returncred=False):
    if returncred:
        imap, cred = connectIMAP(returncred=returncred)
    else:
        imap = connectIMAP()
    folders =  imap.list()[1]
    folders = [ f.decode('utf-8').replace('"','').split('/ ')[1] for f in folders ]

    if folder not in folders:
        print("Requested folder",folder,"not in mail box... using inbox")
        folder = 'inbox'
    (status, response_text) = imap.select('"{}"'.format(folder))
    message_count = int(response_text[0].decode("ascii"))
    print("Found",message_count,"messages")

    result, data = imap.uid('search', None, "ALL")
    if result != 'OK':
        print("Problem getting emails...")
        sys.exit()
    
    mails = []
    datalist = data[0].split()
    datalist.reverse()
    datalist = datalist[0:mailmax]
    for num in datalist:
        result, data = imap.uid('fetch', num, '(RFC822)')
        if result != 'OK':
            print("Problem getting mail",num)
            continue
        email_message = email.message_from_bytes(data[0][1])
        subject = email_message['Subject'] 
        if isinstance(email_message.get_payload(), str):
            content = email_message.get_payload()
        else:
            content = str(email_message.get_payload()[0])
        
        if subject is None: continue
        if "RE:" in subject: continue
        if "Re:" in subject: continue


        details = ""
        details += "Subject: "+str(subject)+"\n"
        details += 'From: '+str(email_message['From'])+"\n"
        details += 'To: '+str(email_message['To'])+"\n"
        details += 'Date: '+str(email_message['Date'])+"\n"
        details += "Content: "+str(email_message.get_payload())

        
        printstr=""
        topic = ""
        source = "?"
        cells = None
        fullcontent = None
        parsed = ""
        runNumber = 999999
        
        if "Event Veto " in subject:
            for run in runNumberList:
                if "Event Veto "+str(run) in subject:
                    runNumber = run
                    topic = "Event Veto for run "+str(run)
                    printstr = "*"*10+" "+subject+" for run "+str(run)                       
                    source, parsed = parseEVMail(email_message.get_payload())
                else: continue
        elif "Missing EVs" in subject:
            for run in runNumberList:
                if "EventVeto"+str(run)+"_Missing.db" in content:
                    runNumber = run
                    topic = "Missing EVs for run "+str(run)
                    printstr = "*"*10+" "+subject+" for run "+str(run)   
                    source, parsed = parseMissingEVMail(content, runNumber)
                    if parsed != "":
                        mails.append( {"runNumber": runNumber, "type":topic, "fullcontent": fullcontent, "source":source, "content":parsed, "cells":cells, "date": email_message['Date'] } )
                else: continue
            parsed = ""
        elif "UPD" in subject and "proposal" in subject:           
            for run in runNumberList:
                if str(run) in content:
                    runNumber = run
                    topic = "Noisy cells update for run "+str(run)
                    if "UPD3" in subject:
                        printstr = "*"*10+" UPD3 proposal for run "+str(run)
                        source = "BLK"
                    elif "UPD4" in subject:
                        printstr = "*"*10+" UPD4 proposal for run "+str(run)
                        source = "ES1"
                    print(printstr)
                    parsed, cells, fullcontent = parseUPDMail(content)
                else: continue

        else: continue
        
        if parsed == "": continue

        mails.append( {"runNumber": runNumber, "type":topic, "source":source, "fullcontent": fullcontent, "content":parsed, "cells":cells, "date": email_message['Date'] } )
        if local:
            print(printstr)
            if isinstance(parsed, dict):
                for k in parsed.keys():
                    print(k, ":", parsed[k])
            else:
                print(parsed)
            
            if cells is not None:
                print("*"*20)
                print("The flagged cells were:")
                print(("\n").join(cells))
        
    if not local:
        if returncred:
            return mails, cred
        else:
            return mails 

    imap.close()
    imap.logout()        


if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument('-r', '--run', dest='runNumber', help='Run number(s) which you would like to get information for', type=int, nargs='+', required=True)
    parser.add_argument('-f', '--folder', dest='folder', default='inbox', help='Folder in your emails where the db expert update requests are found.')
    args = parser.parse_args()

    getMessages(runNumberList=args.runNumber, folder=args.folder, local=True)
